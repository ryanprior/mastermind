module Main where

import Control.Monad

import Data.Char
import Data.List

import System.IO

import GHC.IO.Encoding

data Color
  = Red
  | Orange
  | Yellow
  | Green
  | Blue
  | Purple
  deriving Eq

type ColorKey = [Color]

-- Add terminal escape codes for colors
instance Show Color where
  show Red    = "\x1B[38;5;160mR\x1B[0m"
  show Orange = "\x1B[38;5;208mO\x1B[0m"
  show Yellow = "\x1B[38;5;228mY\x1B[0m"
  show Green  = "\x1B[38;5;76mG\x1B[0m"
  show Blue   = "\x1B[38;5;69mB\x1B[0m"
  show Purple = "\x1B[38;5;171mP\x1B[0m"

instance Read Color where
  readsPrec _ ('R':rest) = [(Red   , rest)]
  readsPrec _ ('r':rest) = [(Red   , rest)]
  readsPrec _ ('O':rest) = [(Orange, rest)]
  readsPrec _ ('o':rest) = [(Orange, rest)]
  readsPrec _ ('Y':rest) = [(Yellow, rest)]
  readsPrec _ ('y':rest) = [(Yellow, rest)]
  readsPrec _ ('G':rest) = [(Green , rest)]
  readsPrec _ ('g':rest) = [(Green , rest)]
  readsPrec _ ('B':rest) = [(Blue  , rest)]
  readsPrec _ ('b':rest) = [(Blue  , rest)]
  readsPrec _ ('P':rest) = [(Purple, rest)]
  readsPrec _ ('p':rest) = [(Purple, rest)]

readColorKey :: String -> ColorKey
readColorKey str = [c1,c2,c3,c4]
  where
    (c1:c2:c3:c4:_) = fmap (read . pure) str

safeReadColorKey :: String -> Either String ColorKey
safeReadColorKey str
  | length str < 4 = Left "Too few colors (four required)!"
  | any (`notElem` "ROYGBP") $ fmap toUpper $ take 4 str = Left "Illicit color (only ROYGBP allowed)!"
  | otherwise = Right $ readColorKey str

data Score = Score
  { scoreBlack :: Integer
  , scoreWhite :: Integer
  }

instance Show Score where
  show (Score numBlack numWhite) = intersperse ' ' $ genericReplicate numBlack '◉' ++ genericReplicate numWhite '○'

data Game = Game { masterKey :: ColorKey, plays :: [ColorKey] } deriving Show

data GameEnd = Win | Lose deriving Show

-- whitePegs masterKey guessKey
whitePegs :: ColorKey -> ColorKey -> Integer
whitePegs _ [] = 0
whitePegs [] _ = 0
whitePegs masterKey (guess:rest)
  = whitePego masterKey guess
    + whitePegs (delete guess masterKey) rest

whitePego :: ColorKey -> Color -> Integer
whitePego colorKey color
  | color `elem` colorKey = 1
  | otherwise             = 0

blackPegs :: ColorKey -> ColorKey -> Integer
blackPegs masterKey guessKey = sum $ zipWith blackPego masterKey guessKey

blackPego :: Color -> Color -> Integer
blackPego color1 color2
  | color1 == color2 = 1
  | otherwise        = 0

score :: ColorKey -> ColorKey -> Score
score masterKey guessKey
  = Score
    { scoreWhite = whitePegs masterKey guessKey - numBlackPegs
    , scoreBlack = numBlackPegs
    }
    where numBlackPegs = blackPegs masterKey guessKey

exampleKey :: ColorKey
exampleKey = [Red, Orange, Yellow, Blue]

makeGame :: ColorKey -> Game
makeGame mKey = Game { masterKey = mKey, plays = [] }

getColorKeyOrDieTrying :: String -> IO ColorKey
getColorKeyOrDieTrying prompt = do
  putStr prompt
  maybeColorKey <- safeReadColorKey <$> getLine
  case maybeColorKey of
    Left err -> do
      putStrLn $ "Error: " ++ err
      getColorKeyOrDieTrying prompt
    Right result -> return result

playGameRound :: Game -> IO (Either GameEnd Game)
playGameRound Game { masterKey = masterKey, plays = plays }
  | masterKey `elem` plays = return $ Left Win
  | length plays >= 10     = return $ Left Lose
  | otherwise = do
    newGuess <- getColorKeyOrDieTrying ""
    putStr $ "\x1B[F\x1B[K" ++ (newGuess >>= show) ++ " "
    print $ score masterKey newGuess
    return $ Right Game { masterKey = masterKey, plays = newGuess : plays }

playGame :: IO GameEnd
playGame = do
  hSetEcho stdout False
  gameMasterKey <- getColorKeyOrDieTrying $ "Choose your unguessable colors (" ++ allColors ++ "):\n"
  hSetEcho stdout True
  putStrLn $ "Gimme your smartest guesses (" ++ allColors ++ "):"
  let
    playGuessing :: Game -> IO GameEnd
    playGuessing game = do
      result <- playGameRound game
      either return playGuessing result
  playGuessing $ makeGame gameMasterKey

allColors :: String
allColors = join $ fmap show [Red, Orange, Yellow, Green, Blue, Purple]

main :: IO ()
main = do
  setLocaleEncoding utf8
  forever $ do
    gameResult <- playGame
    putStrLn $ case gameResult of
      Win -> "Guesser: Glorious VICTORY! 😤😤😤"
      Lose -> "Guesser: Crushing DEFEAT! 😱😱😱"
