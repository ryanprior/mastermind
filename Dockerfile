FROM haskell:8

RUN mkdir -p /src/mastermind
WORKDIR /src/mastermind

COPY package.yaml stack.yaml README.md ./
RUN stack setup && stack update

COPY src ./src/
RUN stack install --ghc-options '-optl-static -fPIC'

FROM scratch
COPY --from=0 /root/.local/bin/mastermind /
CMD ["/mastermind"]
